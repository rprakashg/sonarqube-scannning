package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"go.uber.org/zap"
)

var (
	addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")
	wait time.Duration
	srv  *http.Server
)

func main() {
	r := mux.NewRouter()
	r.Methods("GET").
		Path("/").
		Name("Home").
		HandlerFunc(homeHandler)

	srv = &http.Server{
		Addr:         *addr,
		WriteTimeout: time.Second * 10,
		ReadTimeout:  time.Second * 10,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		log.Infof("Server listening on port %s \n", *addr)
		if err := srv.ListenAndServe(); err != nil {
			log.Errorf("Error starting the server %v", zap.Any("exception", err))
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	srv.Shutdown(ctx)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	json := `{
		"message": "Helloworld"
	}`

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(json))
}
